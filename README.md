# Supplemental Electronic Material to Youssef et al. (2024)

# Nonstationary Shear-Wave Velocity Randomization Approach to Propagate Small-Scale Spatial Shear-Wave Velocity Heterogeneities into Seismic Response.

# Youssef, E., Cornou, C., Youssef Abdel Massih, D., Al-Bittar, T. 2024. J Geotech Geoenviron Eng; 150(10):04024102. https://doi.org/10.1061/JGGEFK.GTENG-11884.

Eliane Youssef; Cécile Cornou; Dalia Youssef Abdel Massih;and Tamara Al-Bittar
Eliane Youssef, Univ. Grenoble Alpes.

Last updated by Eliane Youssef (eliane.youssef@univ-grenoble-alpes.fr), September 9 2024 

------------------------------------------------------------------------

#### Outline of this README

- Notes and usage
- Contents and workflow
- Requirements
- Authors and acknowledgments
- License
- Status and support
- References

------------------------------------------------------------------------

#### Notes and usage

- This Supplemental Electronic Material contains two folders in which the Random Field (RF) quantification and the RF discretization codes are separately contained (1D_RF_Quantification_Matlab_codes ; 1D_RF_Discretization_Matlab_codes).

- The codes are used to quantify the variability of a given variable (i.e., find the statistical parameters) in space and propagate this variability into random samples of the variable in question (i.e., random fields).

- The provided codes are strictly used for one-dimensional (1D) applications, as performed in the paper manuscript. However, codes for 2D  applications are equally available from the corresponding author (EY) upon request. Codes for 3D applications are available, however they are being currently updated.

- This package of codes is soon to be uploaded and maintained via a GitLab repository. The repository is hosted at `https://gricad-gitlab.univ-grenoble-alpes.fr/youssefe/variability-quantification-and-discretization-of-random-fields`
  The full address is provided in the published paper, so make sure to visit the GitLab for the latest version. 

- The original code for the discretization of a RF following the Expansion Optimal Linear Estimation (EOLE) method is available online for public use under Finite Element Reliability using Matlab (FERUM) Version 3.0 (FERUMRandomfield). FERUM 4.1  is now a general purpose structural reliability code. This open-source Matlab® toolbox represents an updated version of FERUM 3.1, initially developed and maintained by Armen Der Kiureghian and Terje Haukaas, with the contributions of many researchers at UC Berkeley. Since then, FERUM has been upgraded by several teams of researchers throughout the years. A new version of this code (FERUM 4.x) and a basic user’s manual can be downloaded at the following address: https://www.sigma-clermont.fr/en/ferum or https://www.sigma-clermont.fr/sites/default/files/atoms/files/FERUM4.1_Users_Guide.pdf

------------------------------------------------------------------------

#### Contents and workflow

The workflow begins with the RF quantification (1D_RF_Quantification_Matlab_codes) 
then follows with the RF discretization (1D_RF_Discretization_Matlab_codes).
 
! Note that the functions 
- `fit_autocorr_Vs.m` 
- `Main_program_1D_RF_EOLE.m`
- `Example_RF_1D.m`
are the only functions that the user needs to add the input data, run the code, and obtain the output results. All these functions contain detailed comments. Remaining functions are called and used within these main functions.


1. 1D_RF_Quantification_Matlab_codes

    • `fit_autocorr_Vs.m`
      This code is used for the quantification of variability for a given parameter (i.e., Shear-wave velocity Vs data)
      It requires as input: 2-column format text file: depth(m) Vs(m/s)
      The output is:
    1. The data trend (if it exists) among 2 options (linear, quadratic) (highest R2)
    2. The experimental autocorrelation function and its best-fitted function among 4 different options (squared exponential, exponential, Cosine Decaying, Exponential Decaying) (highest R2)
    3. The Probability distribution function and coefficient of variation using dfittool 


    • `Mann_Kendall.m` 
      A function called by `fit_autocorr_Vs.m` . It performs the non-parametric Mann-Kendall test to check the stationarity of the data. 

For the example provided (TEST_VS_L9.txt) corresponding to layer 9 in the CH2 VS profile at Mirandola site, the user should well retrieve the results using the codes (e.g., Best-fitted Cosine Decaying or Exponential Decaying ACF; Scale of fluctuation ~ 3 m; Average Vs 420 m/s, and CoVVs 5.8%).
- `VS_T6.bmp` : Complete CH2 Vs profile alongside a quadratic trend (H=1 and rsquare = 0.9053 in gofquad).
- `ACF_T6-L9.bmp` : Empirical ACF of Vs data corresponding to Layer 9 of the CH2 Vs profile, 2 good fitted theoretical ACF, and their scale of fluctuation.
- `PDF_T6-L9.bmp` : Empirical histogram of Vs data corresponding to Layer 9 of the CH2 Vs profile, fitted log-normal probability distribution, and Vs spatial statistics. Note that no trend exists for layer 9 (H=0). 



2. 1D_RF_Discretization_Matlab_codes

    • `Main_program_1D_RF_EOLE.m`
      This code is used for the discretization of a Random Field (RF) for a given parameter (i.e., Shear-wave velocity Vs data)
      It requires as input: 
    1. Input data for the RF toolbox that the user must enter in the function `Example_RF_1D.m` called by this main program
    2. Discretization step Δl controlled by the finite element/ finite difference model for the wave propagation in the case of Vs :
       The grid size must respect at least 3 points in each autocorrelation length: Δl < θz/2 but 5 points are recommended (e.g., max step 0.5 m for a 2 m autocorrelation length θz).
       The grid size must respect the wave propagation condition: Δl < Vsmin/c*fmax with c a constant depending on the FE/FD software (e.g., max step 0.5 m for Vsmin = 100 m/s, fmax=20 Hz, and c=10).
    3. Number of desired RF realizations.
      
      The output is:
    1. The RF parameters of each layer stored in TEST_1D_NS_LAYx for example for soil layer x.
    2. The matrix of all the RF realizations of layer x at a step Δl.
    3. RF structure that contains all input parameters and computed eigen values (Eigs) and eigen vectors (Phi) of the correlation matrix.
Note that the discretization is performed separately for each layer. It begins at the top surface of soil: layer 1 is the top layer beginning at vertical coordinate 0 (moving in the negative y-direction)


    • `Example_RF_1D.m`
      Input data for the random field toolbox with 1D random fields (called by `Main_program_1D_RF_EOLE.m` )
      To be defined by the user:
    1. ybdr : additional thickness to be added both on top and bottom of each soil layer to avoid high computation errors on the domain borders
    2. a and b : coordinates a (top depth) and b (bottom depth) in meters fot the current layer in your soil profile
    3. RFinput.Type : probability distribution function type of your Vs data
    4. TheCOV and TheMean : coefficient of variability (Vs standard deviation/Vs mean) and the mean value (m/s) of Vs data in current layer 
    5. RFinput.CorrType : 1D autocorrelation function type
    6. RFinput.para1 and RFinput.para2 : the autocorrelation parameters a (or called para1) and b (or called para2) used for the computation of the autocorrelation length (obtained from the fit_autocorr quantification code)
    7. RFinput.Npts : Number of points used for the computation of the RF, the code automatically computes it as 5 points in each autocorrelation length
    8. RFinput.OrderExp : Order of expansion N of each realization of the RF. N ensures that the variance of the error, that is the difference between the variance of the discretized field and that of the field of origin, is smaller than a certain limit "e". e is taken equal to 10% by Sudret and Der Kiureghian (2000). 


    • `CorrFunEval.m`
      Function to compute the autocorrelation function between all points in the space. It takes the parameters a and b defined in `Example_RF_1D.m`. 


    • `DiscGaussianRandomField.m`
      Function to compute data for gaussian random field discretization. It defines the discretization domain, assigns the RF input data and computes the correlation matrix (calls `CorrFunEval.m`) and eigen values (Eigs) and eigen vectors (Phi) of the correlation matrix (calls `eigs.m`).


    • `DiscRandomField.m`
      Function to compute data for lognormal random field discretization. It computes the mean and Stdv of the underlying Gaussian field then calls the function `DiscGaussianRandomField.m`.


    • `eigs.m`
      Function to solve the eigenvalue problem A*v = lambda*v or the generalized eigenvalue problem A*v = lambda*B*v.  Only a few selected eigenvalues, or eigenvalues and eigenvectors, are computed.


    • `eole.m`
      The EOLE discretization scheme. This code is a free software under the terms of the GNU General Public License as published by the Free Software Foundation; It is developed by the Pacific Earthquake Engineering (PEER) Center. It is available under Finite Element Reliability using Matlab (FERUM) Version 3.0. FERUM 4.1  is now a general purpose structural reliability code. This open-source Matlab® toolbox represents an updated version of FERUM 3.1, initially developed and maintained by Armen Der Kiureghian and Terje Haukaas, with the contributions of many researchers at UC Berkeley.
       

    • `erreur.m`  and `EstimateAccuracy.m`
      Function to compute and plot the variance error that is the difference between the variance of the discretized field and that of the field of origin. It calls the function `EstimateAccuracy.m`.


    • `EvalBasisFunction.m`  and `EvalLNBasisFunction.m`
      A discretized random filed is represented by :
      H_app(x) = mu + Sum_{i=1..OrderExp)  h_j(x) Xi_j
      where mu : mean value of homogeneous field
      OrderExp : number of terms in series expansion
      Xi_j : uncorrelated N(0,1) variates
      h_j(x) : some deterministic basis function depending on the discretization scheme.
      This function evaluates the n-th basis function h_n at point X:
      RF : the random field structure, obtained by DiscRandomField
      X : the current point
      n : the basis function ID


    • `EvalRandomField.m`
      This function evaluates a realization of a discretized random field at point X.
      RF : the random field structure, obtained by DiscRandomField
      X : the current point
      Xi : the vector of outcome of N(0,1) appearing in the expansion : 
      H_app(x) = mu + Sum_{i=1..OrderExp)  h_j(x) Xi_j
      NB : if the field is lognormal, this function returns exp(mu + Sum_{i=1..OrderExp)  h_j(x) Xi_j)


    • `PlotRealization.m`
      This function plots a random field realization.
      

    • `symmmd.m`
      This function is used by the correlation matrix computation.


For the example provided in the codes corresponding to layer 1 in the CH2 VS profile at Mirandola site, the input data for the user is:

- NB_REALIZ = 200; the required number of RF realizations.
- STEP = 0.2; TOP_DEPTH = 0.05; BOT_DEPTH = 4; so that YDEF = [0.05 0.2:0.2:4]; Layer coordinates [top_depth_layer:Δl:bottom_depth_layer]
- ybdr = 5; Additional thickness to minimize borders error
- a = 0 ; and b = 4 +2*ybdr = 14; layer 1 between 0 and 4 m depth
- RFinput.Type = 'Lognormal'; the probability distribution function
- TheCOV = 0.012 ; the coefficient of variation of Vs
- TheMean = 173 ; Mean Vs in m/s
- RFinput.CorrType = 'exp2' ; assuming exponential squared ACF
- RFinput.para1 = 2 ; Autocorrelation length of 2 m
- RFinput.Npts =  36 ; Npts=([(14-0)/2]*5) +1= 36
- RFinput.OrderExp = 50; Order of expansion N of each realization of the RF, 50 gives mean variance error of 2.378e-12, way below the threshold of 10%.


The RF input parameters are saved in `TEST_1D_NS_LAY1.mat` and the 200 Vs RF realizations are saved in `Var_1D_NS_LAY1.mat`
The user should well retrieve the results using the codes.

------------------------------------------------------------------------

#### Requirements

Required Matlab® & Simulink toolbox:
  - Signal Processing Toolbox
  - Econometrics toolbox
  - Curve Fitting Toolbox
  - Distribution Fitter app

------------------------------------------------------------------------

#### Authors and acknowledgments

This research benefited from the support and funding provided by the Research Institute for Development- IRD (France) through the ARTS program, the Lebanese University and the University of Grenoble Alpes.

This code was regularly updated and modified by several collaborators throughout the years, among which: Tamara Al-Bittar; Dalia Youssef Abdel Massih; Nancy Salloum; Elias El-Haber and Eliane Youssef.

------------------------------------------------------------------------

#### License

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

A copy of the GNU General Public License is found in the LICENSE file provided in this collection of program files. 

------------------------------------------------------------------------

#### Status

This package of codes is susceptible to evolve with time, based on future developments and users' feedback. 

------------------------------------------------------------------------

#### Support

In case of questions or problems, do not hesitate to contact the author (EY), eliane.youssef@univ-grenoble-alpes.fr, and please feel free to provide feedback!

------------------------------------------------------------------------

#### References

- Bourinet, J.-M., Mattrand, C., Dubourg, V. (2009). A review of recent features and improvements added to FERUM software. In: Proc. 10 th International Conference on Structural Safety and Reliability (ICOS- SAR 2009), Osaka, Japan, September 13–17, 2009. Ed. by H. Furuta, D.M. Frangopol, M. Shinozuka. CRC Press. (8 pages).

- El Haber, E. (2018). Effects of the spatial variability of soil properties on the variability of surface ground motion. PhD Thesis, Université Grenoble Alpes, Grenoble, France. (In French).

- El Haber, E., Cornou, C., Jongmans, D., Youssef Abdelmassih, D., Lopez-Caballero, F., & AL-Bittar, T. (2019). Influence of 2D heterogeneous elastic soil properties on surface ground motion spatial variability. Soil Dynamics and Earthquake Engineering,123, 75–90. https://doi.org/10.1016/j.soildyn.2019.04.014

- Salloum, N. (2015). Evaluation of the spatial variability of geotechnical soil parameters from geophysical measurements: application to the alluvial plain of Nahr-Beirut (Lebanon). PhD Thesis, Université Grenoble Alpes, Grenoble, France. (In French).

- Sudret, B., & Der Kiureghian, A. (2000). Stochastic Finite Element Methods and Reliability A State-of-the-Art Report. Berkley, California: Department of Civil and Environmental Engineering, Univ. of California.

- Youssef, E. (2023). Simulation of the seismic response in complex media: accounting for the soil spatial variability using non-stationary random fields and application to Beirut city (Lebanon). PhD Thesis, Université Grenoble Alpes, Grenoble, France. (In English).

- Youssef, E., Cornou, C., Youssef Abdel Massih, D., & Al-Bittar, T. (2021). A non-stationary probabilistic model for generating random fields of shear wave velocity profiles for ground response analyses. In Proc., 6 th IASPEI /IAEE International Symposium on the Effects of Surface Geology on Seismic Motion (ESG6), Kyoto, Japan, 2021.

- Youssef, E., Cornou, C., Youssef Abdel Massih, D., & Al-Bittar, T. (2024). Non-stationary shear-wave velocity randomization approach to propagate small-scale spatial shear-wave velocity heterogeneities into seismic response. Journal Geotechnical Geoenvironmental Engineering; 150(10):04024102. https://doi.org/10.1061/JGGEFK.GTENG-11884.

- Youssef, E., Cornou, C., Youssef Abdel Massih, D., Al-Bittar, T., Yong, A., & Hollender, F. (2024). Application of non-stationary shear-wave velocity randomization approach to predict 1D seismic site response and its variability at two downhole array recordings. Soil Dynamics and Earthquake Engineering; 186: 108945. https://doi.org/10.1016/j.soildyn.2024.108945

------------------------------------------------------------------------

