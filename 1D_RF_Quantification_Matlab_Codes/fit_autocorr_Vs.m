%% This code is used for the quantification of variability for a given parameter (i.e., Shear-wave velocity Vs data)

% It requires as input: 2-column format text file: depth(m) Vs(m/s)
% The output is:
% 1. The data trend (if it exists) among 2 options (linear, quadratic) (highest R2)
% 2. The experimental autocorrelation function and its best-fitted function among 4 different options (squared exponential, exponential, Cosine Decaying, Exponential Decaying)(highest R2)
% 3. The Probability distribution function and coefficient of variation using dfittool (More details are provided below)

clear
clc;
set(0,'defaulttextfontsize',12);
set(0,'defaulttextfontname','Times New Roman');

% Load input Vs data in 2-column format: depth(m) Vs(m/s)
load TEST_VS.txt % or TEST_VS_L9.txt

% Assign your Vs data to variable name VS_DATA
VS_DATA = TEST_VS;
% Assign variables to data
Dist = VS_DATA(:,1); % distance with depth
Vs = VS_DATA(:,2);   % Vs profile values
Size = size(VS_DATA(:,1)',2);

%% Trend computation and removal (if any)

% Perform the non-parametric Mann-Kendall test to check the stationarity of Vs data 
% H=0 is the null hypothesis that the data come from independent realizations (no trend)
% H=1 is the alternative hypothesis that the data follows a monotonic trend
% p_value is the significance chosen as 1% in general
[H,p_value] = Mann_Kendall(Vs,0.01)

% Plot Vs vs Dist alongside both the linear and quadratic trends using NonLinear least square regression
figure(1);
plot(Vs,Dist,'.k');

s0l = fitoptions('Method','NonlinearLeastSquares','Robust','on',...
'Startpoint',[0 0],'TolX',1e-7);

 s0q = fitoptions('Method','NonlinearLeastSquares','Robust','on',...
'Startpoint',[0 0 0],'TolX',1e-7);

flin = fittype('a*x+b','options',s0l); %Linear polynomial curve
fquad = fittype('a*x^2+b*x+c','options',s0q); %Quadratic polynomial curve

% Test which trend fits best with Vs data: Linear or Quadratic
% Linear trend fits better if goflin > gofquad
% Quadratic trend fits better if goflin < gofquad
[clin,goflin] = fit(Dist,Vs,flin); 
[cquad,gofquad] = fit(Dist,Vs,fquad);
trendlin = feval(clin,Dist);
trendquad = feval(cquad,Dist);

hold on;
plot(trendlin, Dist,'r-','LineWidth',2);
hold on;
plot(trendquad, Dist,'r--','LineWidth',2);
set(gca,'YDir','reverse')
legend('Vs Data', 'Linear trend','Quadratic trend'); 
ylabel('Z(m)','fontsize',12,'fontweight','b');
xlabel('Vs(m/s)','fontsize',12,'fontweight','b');

% Remove the spatial trend using the least square regression: If the data follows a certain trend, after removing the trend, stationarity and homogeneity are achieved
% Normalize the fluctuations about mean values
if H==1 %When a trend exists
   % Put cquad for the quadratic trend and clin for the linear trend
   trend = feval(cquad,Dist); %!!!Use clin for linear trend!!
   sigmatrend = std(trend);
   Vsdet = Vs-trend;
   Vsnorm = Vsdet/sigmatrend;
   [cquadnorm,gofquadnorm] = fit(Dist,Vsnorm,fquad); %!!!Use clinnorm and goflinnorm for linear trend
   trendnorm = feval(cquadnorm,Dist); %!!!Use clinnorm for linear trend
   
   % Plot de-trended and normalized Vs data
   figure(2);
   plot(Vsnorm,Dist,'.k');
   hold on;
   plot(trendnorm,Dist,'r','LineWidth',2);
   set(gca,'YDir','reverse')
   ylabel('Z(m)','fontsize',12,'fontweight','b');
   xlabel('Vs Norm','fontsize',12,'fontweight','b');
   legend('Detrended Vs', 'Trend'); 

% In case there is no trend for Vs data remove the mean value from Vs data
else 
   m = mean(Vs);
   sigma = std(Vs);
   Vsdet = Vs-m;
   Vsnorm = Vsdet/sigma;

   % Plot de-trended and normalized Vs data
   figure(2);
   plot(Vsnorm,Dist,'.k');
   set(gca,'YDir','reverse')
   ylabel('Z(m)','fontsize',12,'fontweight','b');
   xlabel('Vs Norm','fontsize',12,'fontweight','b');
   legend('Detrended Vs');

end  

%% Experimental Autocorrelation function computation and fitting to theoretical ACF

% Autocorrelation function fitting
YY=VS_DATA(:,1)- VS_DATA(1,1); % Step computation
[ACF_y,Lags_y,Bounds] = autocorr(Vsdet,Size-1,[],[]); % Lags_y=length(Series)-1
% [ACF_y,Lags_y,Bounds] = autocorr(Vs,Size-1,[],[]); % Lags_y=length(Series)-1

% Plot the Autocorrelation function and find a fit using NonLinear Least square regression
figure(3);
plot(YY,ACF_y,'.k','LineWidth',2);
hold on
s1 = fitoptions('Method','NonlinearLeastSquares',...
'Startpoint',2);
s2 = fitoptions('Method','NonlinearLeastSquares',...
'Lower',0,...
'Startpoint',0.2);
s3 = fitoptions('Method','NonlinearLeastSquares',...
'Startpoint',[0.4 0.4]);
s4 = fitoptions('Method','NonlinearLeastSquares',...
'Lower',[0 0],...
'Startpoint',[5 5]);
f1 = fittype('exp(-(x/a)^2)','options',s1); %squared exponential function
f2 = fittype('exp(-(1/a)*abs(x))','options',s2); %exponential function
f3 = fittype('exp(-b*x)*cos(a*x)','options',s3); %cosine decaying function
f4 = fittype('cos((b2+1)*atan(x/b1))/((1+(x/b1)^2)^((b2+1)/2))','options',s4); %exponential Decaying function

% Fit the computed experimental ACF to predefined ACFs taking the whole domain depth
% Analytical expressions are fitted to the sample autocorrelation functions using regression analysis based on least square error approach. 
% The least square error is generally characterised by the determination coefficient of the fit. 
% Note1: Compare the resulting gof-y values, the ACF holding the highest one is considered to be the best fitted ACF to the computed ACF 
% Note2: Once the best fitted ACF has been assigned, the corresponding correlation length parameters a (or a and b) are stored in c-y
[c1y,gof1y] = fit(YY,ACF_y,f1); %squared exponential function (scale of fluctuation = a*sqrt(pi))
[c2y,gof2y] = fit(YY,ACF_y,f2); %exponential function (scale of fluctuation = a*) 
[c3y,gof3y] = fit(YY,ACF_y,f3); %cosine decaying function (scale of fluctuation = 2a/(b^2+a^2))
[c4y,gof4y] = fit(YY,ACF_y,f4); %exponential Decaying function (scale of fluctuation = a*tan(pi/2(b+1)))

plot(c1y,'k:'); %squared exponentiel function
hold on
plot(c2y,'--'); %exponentiel function
hold on
plot(c3y,'k-.'); %cosine Decaying function
hold on
plot(c4y) %exponential Decaying function
hold on
xlabel('Distance (m)','fontsize',12,'fontweight','b');
ylabel('Autocorrelation','fontsize',12,'fontweight','b');
legend('Vs Data','Exponential Square','Exponential','Cosine Decaying','Exponential Decaying') 

%% Estimate spatial statistics: Probability distribution function and coefficient of variation using dfittool

% Import Vs data from Workspace and create a new probability distribution fit
% Plot the Vs data histogram and fit it to an empirical probability distribution
% The Distribution Fitting Tool determines the extent of the data (nonnegative, unit interval, etc.) and displays appropriate distributions in the Distribution drop-down list. 
% Find the probability parameters by minimizing the log of the probability density function (To find the bestfitted probability distribution) (Fitting method: Maximum likelihood = minimizing its opposite natural logarithm)
% Note: The COV of a variable is defined as the ratio between standard deviation and mean of the variable. 
dfittool
