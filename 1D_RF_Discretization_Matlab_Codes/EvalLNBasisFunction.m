function f = EvalLNBasisFunction(RF,X,Psi_n,Norm2Psi_n)
%
%  f =   EvalLNBasisFunction(RF,X,Psi_n,Norm2Psi_n)
% 
%  A discretized Lognormal random field can be expanded onto the polynomial chaos as:
%  L_app(x) = mu_l + Sum_{i=1..OrderExpLN)  l_j(x) Psi_j
%  
%	 where mu_l      : mean value of homogeneous lognormal field
%		   OrderExpLN : number of terms in series expansion
%		   Psi_j      : j-th polynomial chaos basis function
%		   l_j(x)     : some deterministic basis function depending on
%					       the discretization scheme.
%
%  This function evaluate the n-th basis function l_n at point X:
%		   RF       : the random field structure, obtained by DiscRandomField
%		   X        : the current point
%		   Psi_n    : the vector (alpha) representing the basis function n
%	      Norm2Psi_n : its square norm
%  
		 
if isequal( Psi_n, zeros([length(Psi_n) , 1]))
  f = RF.LNMean;
else
  l_n =1;
  for i =1 : length(Psi_n)
	if Psi_n(i) > 0 
	  l_n = l_n * (EvalBasisFunction(RF,X, i))^Psi_n(i) ;
	end
  end
  f = l_n * RF.LNMean / Norm2Psi_n;
end
return

