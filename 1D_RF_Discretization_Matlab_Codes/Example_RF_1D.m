%% Input data for the random field toolbox with 1D random fields
% To be defined by the user:
% 1. ybdr : additional thickness to be added both on top and bottom of each soil layer to avoid high computation errors on the domain borders
% 2. a and b : coordinates a (top depth) and b (bottom depth) in meters fot the current layer in your soil profile
% 3. RFinput.Type : probability distribution function type of your Vs data
% 4. TheCOV and TheMean : coefficient of variability (Vs standard deviation/Vs mean) and the mean value (m/s) of Vs data in current layer 
% 5. RFinput.CorrType : 1D autocorrelation function type
% 6. RFinput.para1 and RFinput.para2 : the autocorrelation parameters a(or called para1) and b(or called para2) used for the computation of the autocorrelation length (obtained from the fit_autocorr quantification code)
% 7. RFinput.Npts : Number of points used for the computation of the RF, the code automatically computes it as 5 points in each autocorrelation length
% 8. RFinput.OrderExp : Order of expansion N of each realization of the RF. N ensures that the variance of the error, that is the difference between the variance of the
% discretized field and that of the field of origin, is smaller than a certain limit "e". e is taken equal to 10% by Sudret and Der Kiureghian,2000. 

%% Quantification data input
% Specify the additional thickness to be added both on top and bottom of each soil layer to avoid high computation errors on the domain borders
% This value will not interfere with your actual layer thickness but only insures that the variance error is low within the layer
% Common values are 2 meters, 5 meters and 10 meters, a higher value decreases the variance error inside your domain boundaries (but increases the calculation time)
ybdr = 5 ;

%Insert coordinates a (top depth) and b (bottom depth) of current layer in your soil profile
a = 0 ; %e.g., a= 0 layer begins at 0 meters (surface), a = 5 layer begins at 5 meters from the surface
b = 4 +2*ybdr ; %e.g., b= 4 + 2*5 = 14 layer ends at 4 meters from the surface with an additional 5 meters from each side of the current layer to avoid the borders error

RFinput.Domain = {a ,b } ;

% Specify the probability distribution function type of your Vs data (e.g., Lognormal (common), Gaussian)
RFinput.Type = 'Lognormal';

% Specify the coefficient of variability (Vs standard deviation/Vs mean) and the mean value (m/s) of Vs data in current layer 
TheCOV = 0.012 ; 
TheMean = 173 ; % m/s

% Checking if distribution is Lognormal to transform the RFinput to the lognormal space
if isequal(RFinput.Type ,'Lognormal') 
   RFinput.LNMean = TheMean  ;
   RFinput.LNStdv = TheCOV * RFinput.LNMean ; 
end
if isequal(RFinput.Type ,'Gaussian') 
   RFinput.Mean = TheMean  ;
   RFinput.Stdv = TheCOV * RFinput.Mean ;
end

% Specify the 1D autocorrelation function type, most used models are
% 1- Exponential 'exp', 2- Squared exponential 'exp2', 3-Cosine decaying function 'cosdec', 4-Exponential decaying 'expdec'
RFinput.CorrType = 'exp2' ;

% Specify the autocorrelation parameters a(or called para1) and b(or called para2) used for the computation of the autocorrelation length (obtained from the fit_autocorr code)
% In the e.g., a (para1) corresponds to an autocorrelation length of 2 m for an exp2 ACF.
RFinput.para1 = 2 ; %For exp and exp2 RFinput.para1=[a]; For expdec and cosdec RFinput.para1=[a or b1]. a or b1 being the parameters taken from quantification
RFinput.para2 = [] ; %For exp and exp2 RFinput.para2 will not be used --> RFinput.para2=[]; For expdec and cosdec RFinput.para2=[b or b2]. b or b2 being the parameters taken from quantification

% Specify the 1D RF discretization method, here the EOLE stands for Expansion Optimal Linear Estimation Method
RFinput.DiscScheme = 'EOLE';

% Specify the Number of points used for the computation of the RF
% Npts is computed by ([domain length (b-a)/Autocorrelation length]*Number of points required in 1 autocorrelation length)+1
% When not specified the code automatically computes RF.Npts as 5 points in each autocorrelation length (recommended)
RFinput.Npts =  36 ; %in the e.g., Npts=([(14-0)/2]*5) +1= 36.

% Specify the Order of expansion N of each realization of the RF. N ensures that the variance of the error, that is the difference between the variance of the
% discretized field and that of the field of origin, is smaller than a certain limit "e". e is taken equal to 10% by Sudret and Der Kiureghian,2000. 
% Increase N when the mean value of e over all the domain is greater than 10%.
RFinput.OrderExp = 30; % Should be below Npts

% Call the function to discretize the RF based on the input parameters
RF = DiscRandomField([],RFinput);

% Plot the difference between the variance of the discretized field and that of the field of origin, notice the higher error at the borders
figure(1);
clf;
EstimateAccuracy(RF,150); %Check the mean error limit "e". 

% figure(2);
% clf;
% PlotRealization(RF,150,[]);

