function p = symmmd(S)
%SYMMMD Symmetric minimum degree permutation.
%   p = SYMMMD(S), for a symmetric positive definite matrix S,
%   returns the permutation vector p such that S(p,p) tends to have a
%   sparser Cholesky factor than S.  Sometimes SYMMMD works well
%   for symmetric indefinite matrices too.
%
%   See also COLMMD, COLPERM, SYMRCM.

%   Copyright 1984-2001 The MathWorks, Inc. 
%   $Revision: 5.7 $  $Date: 2001/04/15 12:00:19 $

% p = sparsfun('symmmd',S);
% [ignore,q] = sparsfun('symetree',S(p,p));
% p = p(q);
p=symamd(S);
