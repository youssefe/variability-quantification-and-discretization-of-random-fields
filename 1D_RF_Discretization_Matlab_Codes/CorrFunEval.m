function f = CorrFunEval(type, x, y, a,b)
%		   
%		  f = CorrFunEval(type, x, y, a,b)
%		  
%		  Compute the autocorrelation function 
%			
%		  type  : 'exp' for exponential, 'exp2' for exponential-square ...
%		  x , y : points of evaluation
%		  a, b : parameters of cosine decaying or exponential decaying correlation function where 
%			correlation length= 2a/(b^2+a^2) for cosine decaying
%			correlation length= a*tan(pi/(2*(b+1))) for exponential decaying
%			correlation length= a for exp and exp2 
%		  
%		  NB    : this function is vectorized, i.e x,y,a,b can be vectors for 2D case
  
  switch type
   case 'exp'
	f = exp(- sum(abs(x-y)./a)) ;
   case 'exp2'
	f = exp(- sum(((x-y).^2)./ (a.^2))) ;
   case 'expdec'
	f = prod((cos((b+1).*atan((x-y)./a)))./((1+((x-y)./a).^2).^((b+1)./2)));
   case 'cosdec'
	f = prod(exp(-b.*(x-y)).*cos(a.*(x-y))) ;
   otherwise 
	fprintf('\n This correlation function type is not supported \n');
	error(' ' );
  end
  
  
