function RF = DiscGaussianRandomField(COORD,RFinput)
%  
%      RF = DiscGaussianRandomField(COORD, RFinput)
%      Compute data for gaussian random field discretization
%  
%  

%%------------------------------------------------------------%%
%            Common initialization for all discretization methods
%%------------------------------------------------------------%%

RF.Type = RFinput.Type ;
RF.Mean = RFinput.Mean ;      % homogeneous random fields are considered
RF.Stdv = RFinput.Stdv ;      

if isfield(RFinput, 'Domain')
   RF.Domain = RFinput.Domain;
   xmin = RF.Domain{1};
   xmax = RF.Domain{2};
else  %    computing automatically the size of discretization domain
   xmin = min(COORD);            
   xmax = max(COORD);
   RF.Domain = { xmin , xmax};
end

%    assigning input data
RF.CorrType   = RFinput.CorrType;
RF.para1 = RFinput.para1;
RF.para2 = RFinput.para2;
RF.DiscScheme = RFinput.DiscScheme;
RF.OrderExp   = RFinput.OrderExp;
RF.Outcome    = zeros([RF.OrderExp , 1]);

OrderGaussExp = RF.OrderExp;

switch RF.DiscScheme

case 'EOLE'
   %%------------------------------------------------------------%%
   %            EOLE expansion of 2D fields
   %%------------------------------------------------------------%%
   
  switch RF.CorrType
    
    case 'exp'
    RF.CorrLength=RF.para1;
   
    case 'exp2'
    RF.CorrLength=RF.para1;
  
    case 'expdec'
    RF.CorrLength=RF.para1.*tan(pi()./((RF.para2+1).*2));
    
    case 'cosdec'   
    RF.CorrLength= 2*RF.para1./(RF.para1.^2+RF.para2.^2); 
  end   
   
   if isfield(RFinput, 'Npts')
      RF.Npts = RFinput.Npts ;	
   else
      fprintf('\n\n NB : Grid not specified for EOLE expansion \n');
      fprintf('      Uniform grid chosen with 5 points in the correlation length\n\n');
      
      switch length(RF.CorrLength)
      case 1
         RF.Npts = 1 + 5 * ceil((xmax(1) -xmin(1))/RF.CorrLength);
         fprintf('      RF.Npts = %3.0d\n\n',RF.Npts);
      case 2
         RF.Npts(1) = 1 + 5 * ceil((xmax(1) -xmin(1))/RF.CorrLength(1));
         RF.Npts(2) = 1 + 5 * ceil((xmax(2) -xmin(2))/RF.CorrLength(2));
         fprintf('      RF.Npts = [%3.0d , %3.0d]\n\n',RF.Npts(1),RF.Npts(2));
      end
   end
   
   switch length(RF.CorrLength)
   case 1
      VV = (xmin(1) : (xmax(1) -xmin(1))/(RF.Npts(1)-1) : xmax(1))';
   case 2
      [A , B ] = meshgrid(xmin(1) : (xmax(1) -xmin(1))/(RF.Npts(1)-1) : xmax(1) , ...
         xmin(2) : (xmax(2) -xmin(2))/(RF.Npts(2)-1) : xmax(2));
      
      Asize = size(A);
      NbRFnodes = Asize(1) * Asize(2);
      VV = cat(2,reshape(A',NbRFnodes,1),reshape(B',NbRFnodes,1));
   otherwise
   end;
   
   CorrMat = zeros(length(VV));
   for i = 1 : length(VV)
      for j = i : length(VV)
         CorrMat(i,j) = CorrFunEval(RF.CorrType , VV(i,:) , VV(j,:) , ...
            RF.para1, RF.para2);
         CorrMat(j,i) = CorrMat(i,j);
      end;
   end;
   %CovMat;
   options.disp = 0;
   %  Verifies CovMat*Phi =  Phi*Theta
   [Phi , Theta] = eigs(CorrMat,eye(size(CorrMat)),OrderGaussExp,options);
   RF.COORD = VV;
   RF.Phi = Phi;              % eigenvectors of correlation/and
   % covariance matrix 
   RF.Eigs = diag(Theta);  % eigenvalues of CorrMatrix order from
   % the largest 
   
otherwise
   fprintf('\n\n NB : Valid discretization schemes are :\n');
   fprintf('         ''EOLE'',\n'); 
   error(' ');
end;
