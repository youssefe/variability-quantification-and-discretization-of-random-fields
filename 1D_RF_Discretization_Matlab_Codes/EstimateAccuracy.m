function Vmean = EstimateAccuracy(RF,Npts)
%
%  Vmean = EstimateAccuracy(RF,Npts)
%  
%  compute the variance error of a discretized gaussian 
%  random field and plot it
%  
%  RF  :  a random field structure obtain by DiscRandomField
%  Npts:  The number of points for plotting
%			1D field : a single integer
%			2D field : [ two integers]
%		   
%  see also : DiscRandomField, DiscGaussianRandomField		   

switch RF.Type
case 'Gaussian'
   clf;
   OrderExp = RF.OrderExp;
   xmin = RF.Domain{1};
   xmax = RF.Domain{2};
   dimPb = length(RF.CorrLength);
   
   switch dimPb
   case 1   
      X = (xmin(1):(xmax(1) - xmin(1))/(Npts(1)-1):xmax(1))';	
      VarH = RF.Stdv * RF.Stdv;    % variance of original field
      VarError= ones(size(X));
      
      fprintf('\n * Computing variance error ...\n      ')
      index = 0 ;
      index_total = ceil(Npts(1) /10) *10;
      
      switch RF.DiscScheme
         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
      case 'KL'
         for q = 1 : Npts(1)     
            index = index + 1;
            if (mod (index , index_total/10) == 0)
               fprintf(' %1.0f0%% /', (10 * index / index_total));  
               % displays the status of simulation  
            end;
            for k = 1 : OrderExp
               VarError(q) = VarError(q) -  ... 
                  (EvalBasisFunction(RF,X(q),k))^2 / VarH;
            end
         end
         
         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
      case 'EOLE'
         for q = 1 : Npts(1)     
            index = index + 1;
            if (mod (index , index_total/10) == 0)
               fprintf(' %1.0f0%% /', (10 * index / index_total));  
               % displays the status of simulation  
            end;
            for k = 1 : OrderExp
               VarError(q) = VarError(q) -  ... 
                  (EvalBasisFunction(RF,X(q),k))^2 / VarH;
            end
         end
         
         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
      case 'OSE'	
         SizeDomain = (RF.Domain{2} - RF.Domain{1})/2;
         NPG  = 16 ;
         [xIP , WGH ] = GaussPoints(NPG);
         LP =  LegendrePolynomials(OrderExp,xIP);     % values of Legendre P. at
         % Gauss points
         for q = 1 : Npts(1)     
            index = index + 1;
            if (mod (index , index_total/10) == 0)
               fprintf(' %1.0f0%% /', (10 * index / index_total));  
               % displays the status of simulation  
            end;
            %		 for k = 1 : OrderExp
            %		   VarError(q) = VarError(q) -  ... 
            %			   (EvalBasisFunction(RF,X(q),k))^2 / VarH;
            %		 end
            His =   OSEBasis(RF,X(q));    % Evaluate H_i function at X(q)
            VarError(q) = 1 + His' * RF.CovChi * His ;
            
            for i =1 : RF.OrderExp
               % compute int {h_i(y) * rho (X(q), y)} dy
               TheInt = 0;
               for k = 1 : NPG
                  TheInt = TheInt + WGH(k) * LP(i , k) * ...
                     CorrFunEval(RF.CorrType, X(q), ...
                     (SizeDomain * xIP(k) + RF.Translation), ...
                     RF.CorrLength);
               end
               TheInt = TheInt * sqrt(SizeDomain * (2*i-1)/2);
               
               VarError(q) = VarError(q) - 2 * TheInt * His(i);
            end
         end
      end
      
      
      fprintf('\n');
      %%------------------------------------------------------------%%
      %      Displaying results
      %%------------------------------------------------------------%%
      
      fprintf('\n      Domain of discretization : [%4.2g , %4.2g]\n',  xmin,xmax); 
      fprintf('      Discretization scheme    : %s\n' , RF.DiscScheme);
      fprintf('      Order of expansion       : %2.0d\n',OrderExp);
      fprintf('      Error Variance           : \n');
      Vmax = max(VarError);
      Vmin = min(VarError); 
      Vmean = mean(VarError);
      fprintf('            Min = %10.4g\n           Mean = %10.4g\n            Max = %10.4g \n', Vmin, Vmean, Vmax);
      
      plot (X, VarError);
      
      
      xlabel ('x'); ylabel('Var [H(x) - H_{app}(x)]');
      title('Error variance along the discretization domain');
      Ys = get(gca, 'Ylim');
      text(xmin + (xmax-xmin)*.05 ,(Ys(2) - .05 * (Ys(2) - Ys(1)))  , ['Discretization scheme : ' , RF.DiscScheme]);
      
      text(xmin + (xmax-xmin)*.05 ,(Ys(2) - .1 * (Ys(2) - Ys(1)))  , ['Order of expansion : ' , ...
            num2str(OrderExp)]); 
      text(xmin + (xmax-xmin)*.1 ,(Ys(2) - .15 * (Ys(2) - Ys(1))), ['Min : ' ,num2str(Vmin)] );
      text(xmin + (xmax-xmin)*.1 ,(Ys(2) - .20 * (Ys(2) - Ys(1))), ['Mean : ' ,num2str(Vmean)] );
      text(xmin + (xmax-xmin)*.1 ,(Ys(2) - .25 * (Ys(2) - Ys(1))), ['Max : ' ,num2str(Vmax)] );
      
      
      
      
   case 2
      
      % Generate grid for representing variance error
      [X , Y] = meshgrid(xmin(1):(xmax(1) - xmin(1))/(Npts(1)-1):xmax(1) , ...
         xmin(2):(xmax(2) - xmin(2))/(Npts(2)-1):xmax(2));
      VarH = RF.Stdv * RF.Stdv;    % variance of original field
      VarError= ones(size(X));    % Error initialized to 1.
      % Starting conputation
      fprintf('   Computing error variance ...\n      ')
      index = 0 ;
      index_total = ceil(Npts(1) * Npts(2) /10) *10;
      for i = 1 : Npts(2)       
         for j = 1 : Npts(1)     
            index = index + 1;
            if (mod (index , index_total/10) == 0)
               fprintf(' %1.0f0%% /', (10 * index / index_total));  
               % displays the status of simulation  
            end;		
            for k = 1 : OrderExp
               VarError(i,j) = VarError(i,j) -  ... 
                  (EvalBasisFunction(RF,[X(1,j) , Y(i,1)],k))^2 / VarH;
            end;
         end;
      end;
      fprintf('\n');
      
      %%------------------------------------------------------------%%
      %      Displaying results
      %%------------------------------------------------------------%%
      fprintf(...
         '      Domain of discretization : [%4.2g , %4.2g] x [%4.2g , %4.2g]\n', ...
         xmin,xmax); 
      fprintf('      Discretization scheme    : %s\n' , RF.DiscScheme);
      fprintf('      Order of expansion       : %2.0d\n',OrderExp);
      fprintf('      Error Variance       : \n');
      
      Vmax = max(max(VarError));
      Vmin = min(min(VarError)); 
      Vmean = mean(mean(VarError));
      fprintf('            Min = %10.4g\n           Mean = %10.4g\n            Max = %10.4g \n', Vmin, Vmean, Vmax);
      
      %%------------------------------------------------------------%%
      %      Plotting Variance of Error over Domain of discretization
      %%------------------------------------------------------------%%
      clf;
      surfl(X,Y,VarError);
      mindim = min(min(xmax -xmin));
      set(gca,'DataAspectRatio', [1 1 Vmax/mindim]);
      colormap(pink);
      zlabel('Var [H(x) - H_app(x)]');
      TheTitle= 'Error variance along the discretization domain';
      title(TheTitle);
      Xs = get(gca, 'Xlim');
      Ys = get(gca, 'Ylim');
      t1 = strcat('Discretization scheme : ' , RF.DiscScheme);
      t2 = strcat('Order of expansion : ' ,num2str(OrderExp));
      text(Xs(1)+ 0.25 * (Xs(2) - Xs(1)) , Ys(1) , t1);
      text(Xs(1)+ 0.25  * (Xs(2) - Xs(1)) , Ys(1) - 0.2 *(Ys(2) - Ys(1)), t2);
      
 
      %   Alternative representation
      %
      %	subplot(2,1,1);
      %	surfl(X,Y,VarError);
      %	mindim = min(min(xmax -xmin));
      %	set(gca,'DataAspectRatio', [1 1 Vmax/mindim]);
      %	
      %	subplot(2,1,2);
      %	contourf(X,Y,VarError,10);
      %	axis('equal','off');
      
   end
case 'Lognormal' 
   RF1 = RF;
   RF1.Mean = RF.LNMean;
   RF1.Stdv = RF.LNStdv;
   RF1.Type = 'Gaussian';
   Vmean = EstimateAccuracy(RF1,Npts);
   fprintf('\n\n NB : Represented is the variance error of the underlying \n');
   fprintf('      Gaussian field\n\n');

   
otherwise
   fprintf('\n\n NB : EstimateAccuracy is only available for Gaussian and Lognormal fields \n\n');
     
end


