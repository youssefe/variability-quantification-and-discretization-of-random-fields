%% This code is used for the discretization of a Random Field (RF) for a given parameter (i.e., Shear-wave velocity Vs data)

% It requires as input: 
% 1. Input data for the RF toolbox that the user must enter in the function `Example_RF_1D.m` called by this main program
% 2. Discretization step controlled by the finite element/ finite difference model for the wave propagation in the case of Vs (more details are provided below)
% 3. Number of RF realizations.

% The output is:
% 1. The RF parameters of each layer stored in TEST_1D_NS_LAYx for example for layer x.
% 2. The matrix of all the RF realizations of layer x at a step Δl.
% 3. RF structure that contains all input parameters and computed eigen values (Eigs) and eigen vectors (Phi) of the correlation matrix.

% Discretization begins at the top surface of soil: layer 1 is the top layer beginning at vertical coordinate 0 (moving in the negative y-direction)
clear
clc; 
% Introduce the global variable xsis that will contain the independent normal standard variables
global xsis 

% Modify here the required number of RF realizations
NB_REALIZ = 200;

% Call the main script in which you will define your domain boundaries and the RF parameters
Example_RF_1D;

%% Discretization step defined by the FINITE ELEMENT/FINITE DIFFERENCE model
% Insert coordinates of current soil layer with constant step (e.g., Δl = 0.2 m): YDEF=[top_depth_layer:Δl:bottom_depth_layer]

% The FE/FD grid size must respect at least 3 points in each autocorrelation length: Δl < θz/2 but 5 points are recommended (e.g., max step 0.5 m for a 2 m autocorrelation length θz)
% The FE/FD grid size must respect the wave propagation condition: Δl < Vsmin/c*fmax with c a constant depending on the FE/FD software (e.g., max step 0.5 m for Vsmin = 100 m/s, fmax=20 Hz, and c=10)

% Note:  Only layer 1 has the format YDEF=[0.05 Δl:Δl:bottom_depth_layer] to place the first point at 0.05 m instead of 0;
% The remaining layers are defined as YDEF=[top_depth_layer:Δl:bottom_depth_layer]
% Each next layer must begin at the bottom depth of the previous layer increased by 1 step e.g., for layer 2 YDEF= [top_depth_layer2+Δl:Δl:bottom_depth_layer2];
STEP = 0.2;
TOP_DEPTH = 0.05; %(instead of 0 for first layer only) 
BOT_DEPTH = 4;

YDEF=[TOP_DEPTH STEP:STEP:BOT_DEPTH]; % Layer 1 coordinates [0.05 Δl:Δl:bottom_depth_layer]
%YDEF=[TOP_DEPTH+STEP:STEP:BOT_DEPTH]; % Remaining Layers coordinates [top_depth_layer:Δl:bottom_depth_layer]
YDEF = YDEF';
%% Discretization of the RF

% Generate the matrix of independent normal standard variables, 3000 is the maximum number of the required RF realizations (chosen randomly here)
% Note that xsis_values.mat is already provided so no need to generate it again
%xsis = randn(RF.OrderExp,3000); % Comment if xsis mat is already generated

% Save the matrix 
% Note: you can generate and save the matrix xsis once for the highest order of expansion N and use it for all the layers RF generation 
% If you choose to generate the xsis matrix once as recommended, remember to comment the xsis generation and saving commands and keep only the `load xsis_values.mat`

%save xsis_values xsis; % Comment if xsis mat is already generated

% Save the current layer RF parameters
save TEST_1D_NS_LAY1 % Rename the structure based on the respective layer

load xsis_values.mat

%%% xsis_Cor = PhiD*((EigsD)^(0.5))*xsis; 

% Generate the RF variables and place each ralization in a text file for a certain number of realizations e.g., 200 here
for realiz = 1:NB_REALIZ 
    realiz_Number  = realiz % Displaying the on-going realization number to check the progress of RF generation
    Xcorr=xsis(:,realiz); % Defining the size of Xcorr matrix
    for yyy = 1:size(YDEF,1)
            y = YDEF(yyy,1)+ ybdr; % The parameter ybdr is defined in the Example_RF_1D.m
            % Evaluate the RF for each layer beginning at layer 1 and store the values in the matrix Var_1D_NS_LAY
            % The user must change LAY name for each layer (e.g., Var_1D_NS_LAY1, Var_1D_NS_LAY2, ...)
            Var_1D_NS_LAY(yyy,realiz)= EvalRandomField(RF,y,Xcorr(1:RF.OrderExp,1)); 
    end

end
Var_1D_NS_LAY= real(Var_1D_NS_LAY); % The user must change LAY name for each layer
% Save the matrix 'Var_1D_NS_LAYx.mat' containing all the RF realizations of layer x at a step Δl
save('Var_1D_NS_LAY1.mat','Var_1D_NS_LAY','-v7.3') 

% Once the user have generated and saved all layers realizations Var_1D_NS_LAY1, Var_1D_NS_LAY2, ..., combine all layers variables in 1 matrix using vertcat and save it
%Var_1D_NS_ALL = vertcat(Var_1D_NS_LAY1,Var_1D_NS_LAY2,...);
%save('Var_1D_NS_ALL.mat','Var_1D_NS_ALL.mat','-v7.3')

