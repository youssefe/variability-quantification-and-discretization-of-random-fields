function  PlotRealization(RF,Npts,Outcome)
%  
%  PlotRandomField(RF,Npts,Outcome)
%  
%  Plots a random field realization
%  
%  RF     : the random field data structure
%  Npts   : The number of points for plotting
%			1D field : a single integer
%			2D field : [ two integers]
%  Outcome : an outcome of the standrad normal variable 
%            (e.g. design point)
%            when [], random numbers are generated


OrderExp = RF.OrderExp;
xmin = RF.Domain{1};
xmax = RF.Domain{2};
dimPb = length(RF.CorrLength);

fprintf('\n * Computing random field realization ...\n');

%  Initialize random numbers and generate an outcome
%  if necessary
if isempty(Outcome)
   randn('state',sum(100*clock))
   for i =1 : 100
      Xi = randn([OrderExp, 1]);
   end
else 
   Xi =Outcome;
end


switch dimPb
case 1   % One-dimensional fields
   X = (xmin(1):(xmax(1) - xmin(1))/(Npts(1)-1):xmax(1))';
   for j = 1 : Npts   
      Y(j) = EvalRandomField(RF,X(j),Xi);
   end;
   plot(X,Y);
   
case 2 % Two-dimensional fields
   [X , Y] = meshgrid(xmin(1):(xmax(1) - xmin(1))/(Npts(1)-1):xmax(1) , ...
      xmin(2):(xmax(2) - xmin(2))/(Npts(2)-1):xmax(2));
   index = 0 ;
   index_total = ceil(Npts(1) * Npts(2) /10) *10;
   for i = 1 : Npts(2)       
      for j = 1 : Npts(1)     
         index = index + 1;
         if (mod (index , index_total/10) == 0)
            fprintf(' %1.0f0%% /', (10 * index / index_total));  
            % displays the status of simulation  
         end;		
         Z(i,j) = EvalRandomField(RF,[X(1,j) , Y(i,1)],Xi);
      end;
   end;
   axis fill;
   surfl(X,Y,Z);
   colormap(pink);
otherwise
   fprintf('\n\n NB : Only 1D and 2D fields can be represented by PlotRealization\n\n');
   
end
fprintf('\n');
title('Random field realization');


