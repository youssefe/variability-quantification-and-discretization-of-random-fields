function [M, A] = eole(xn,x,meanfunc,covariancefunc,nterm,display)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Finite Element Reliability using Matlab, FERUM, Version 3.0       %
%                                                                   %
% This program is free software; you can redistribute it and/or     %
% modify it under the terms of the GNU General Public License       %
% as published by the Free Software Foundation; either version 2    %
% of the License, or (at your option) any later version.            %
%                                                                   %
% This program is distributed in the hope that it will be useful,   %
% but WITHOUT ANY WARRANTY; without even the implied warranty of    %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the     %
% GNU General Public License for more details.                      %
%                                                                   %
% A copy of the GNU General Public License is found in the file     %
% <gpl.txt> following this collection of program files.             %
%                                                                   %
% Developed under the sponsorship of the Pacific                    %
% Earthquake Engineering (PEER) Center.                             %
%                                                                   %
% For more information, visit: http://www.ce.berkeley.edu/~haukaas  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Initialize
node = length( xn );
n = length( x );
first = node - nterm + 1;

% Covariance between points 'x' and 'xn'
node = length( xn );
n = length( x );
for i = 1 : node
   for j = 1 : n
      Cnx( i, j ) = eval(covariancefunc);
   end
end

% Covariance between points 'xn' and 'xn'
x = xn;
node = length( xn );
for i = 1 : node
   for j = 1 : node
      Cnn( i, j ) = eval(covariancefunc);
   end
end

% Solve for the eigevalues/vectors of the nodal covariance matrix
[PHI,theta] = eig(Cnn);



if display == 'no '
   
   % Compute matrix multiplication (Eq. 29 in "Opt. Discr. of R.F. by Li & ADK)
   A = Cnx'*PHI( :, first : node )*sqrt(inv(theta( first : node, first : node )));
   
   % Mean
   for i = 1 : n
      M(i,1) = eval(meanfunc);
   end
   
   
else % Give user information about eigenvalues
   
   eigenvalues_theta = diag(theta);
   negative_eigenvalue_flag = 0;
   for i = 1 : node
      if eigenvalues_theta(i) < 0
         negative_eigenvalue_flag = 1;
      end
   end
   if negative_eigenvalue_flag == 1;
      disp('EIGENVALUES OF THE NODAL COVARIANCE MATRIX (random field option):');
      format long
      Eigenvalues = (eigenvalues_theta' * flipud(eye(node)))'
      format short
      disp('Negative eigenvalue encountered. The correlation matrix at nodal points');
      disp('is not positive definite, or is singular. Make sure the correlation function');
      disp('that you use is valid. If so, increase the size of random field elements.');
      M = 0; % Indicates that program should break.
      A = 0;
   else
      disp('EIGENVALUES OF THE NODAL COVARIANCE MATRIX (random field option):');
      format long
      Eigenvalues_in_descending_order = (eigenvalues_theta' * flipud(eye(node)))'
      format short
      M = 1; % Indicates that program may continue on users initiative.
      A = 0;
   end
   
end
